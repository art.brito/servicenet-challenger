﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceQueryAPI.Model
{
    public class Customer
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string mongoID { get; set; }

        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("age")]
        public int Age { get; set; }

        public Customer()
        {
        }

        public Customer(string id, string name, int age)
        {
            this.id = id;
            Name = name;
            Age = age;
        }

    }
}
