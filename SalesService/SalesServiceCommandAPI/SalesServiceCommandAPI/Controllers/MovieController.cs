﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SalesServiceCommandAPI.Command;
using SalesServiceCommandAPI.Model;
using SalesServiceCommandAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SalesServiceCommandAPI.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {

        private MovieService service;

        public MovieController()
        {
            this.service = new MovieService();
        }

        [HttpPost]
        public Movie CreateMovie([FromBody]Movie movie)
        {
            CreateMovieCommand createMovieCommand = new CreateMovieCommand(movie);

            QueuedCommandResult<Movie> commandResult = service.sendCommand(createMovieCommand);

            if (commandResult.isSuccess)
            {
                return commandResult.entity;
            } else
            {
                throw new Exception("Erro ao criar o filme");
            }
        }

        [HttpPut("{id}")]
        public Movie UpdateMovie(string id, [FromBody] Movie movie)
        {
            movie.id = new Guid(id);

            UpdateMovieCommand updateMovieCommand = new UpdateMovieCommand(movie);

            QueuedCommandResult<Movie> commandResult = service.sendCommand(updateMovieCommand);

            if (commandResult.isSuccess)
            {
                return commandResult.entity;
            }
            else
            {
                throw new Exception("Erro ao atualizar o filme");
            }
        }

        [HttpDelete("{id}")]
        public void DeleteMovie(string id)
        {
            DeleteMovieCommand deleteMovieCommand = new DeleteMovieCommand(id);

            QueuedCommandResult<Movie> commandResult = service.sendCommand(deleteMovieCommand);

            if (!commandResult.isSuccess)
            {
                throw new Exception("Erro ao deletar o filme");
            }
        }

    }
}