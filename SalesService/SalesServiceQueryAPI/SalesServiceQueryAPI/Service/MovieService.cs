﻿using SalesServiceQueryAPI.DataBase;
using SalesServiceQueryAPI.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceQueryAPI.Service
{
    public class MovieService
    {

        private DatabaseContext databaseContext;

        public MovieService()
        {
            this.databaseContext = DatabaseContext.getInstance();
        }

        public List<Movie> GetMovies()
        {
            return databaseContext.Movie.Find(new BsonDocument()).ToList();
        }

        public Movie GetMovie(string id)
        {
            return databaseContext.Movie.Find(m => m.id == id).FirstOrDefault();
        }
    }
}
