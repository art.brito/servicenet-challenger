package main

import (
	"database/sql"
	"fmt"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbname   = "crud-database"
)

func updateRelationalDatabase(command MovieCommand) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	db, err := sql.Open("postgres", psqlInfo)
	failOnError(err, "Erro ao conectar-se ao banco de dados")

	switch command.CommandType {
	case CREATE:
		sqlStatement := `INSERT INTO movie (id, title, subTitle, releaseDate)
		VALUES ($1, $2, $3, $4)`
		_, err = db.Exec(sqlStatement, command.Movie.ID, command.Movie.Title, command.Movie.SubTitle, command.Movie.ReleaseDate)
	case UPDATE:
		sqlQueryStatement := `SELECT * FROM movie WHERE id = $1`

		var title string
		var subtitle string
		var releasedate string

		row := db.QueryRow(sqlQueryStatement, command.Movie.ID)
		err := row.Scan(&command.Movie.ID, &title, &subtitle, &releasedate)

		if command.Movie.Title == "" {
			command.Movie.Title = title
		}

		if command.Movie.SubTitle == "" {
			command.Movie.SubTitle = subtitle
		}

		if command.Movie.ReleaseDate == "" {
			command.Movie.ReleaseDate = releasedate
		}

		fmt.Println(command.Movie.ID, command.Movie.Title, command.Movie.SubTitle, command.Movie.ReleaseDate)

		failOnError(err, "Erro ao atualizar registro")

		sqlUpdateStatement := `UPDATE movie 
						 SET title = $2,
							 subtitle = $3,
							 releasedate = $4
						 WHERE id = $1`
		_, err = db.Exec(sqlUpdateStatement, command.Movie.ID, command.Movie.Title, command.Movie.SubTitle, command.Movie.ReleaseDate)
	case DELETE:
		sqlStatement := `DELETE FROM movie WHERE id = $1`
		_, err = db.Exec(sqlStatement, command.Movie.ID)
	}

	failOnError(err, "Erro ao executar")

	defer db.Close()
}
