﻿using MongoDB.Bson;
using MongoDB.Driver;
using SalesServiceQueryAPI.DataBase;
using SalesServiceQueryAPI.Model;
using System.Collections.Generic;
using System.Linq;

namespace SalesServiceQueryAPI.Service
{
    public class CustomerService
    {

        private DatabaseContext databaseContext;

        public CustomerService()
        {
            this.databaseContext = DatabaseContext.getInstance();
        }

        public List<Customer> GetMovies()
        {
            return databaseContext.Customer.Find(new BsonDocument()).ToList();
        }

        public Customer GetMovie(string id)
        {
            return databaseContext.Customer.Find(m => m.id == id).FirstOrDefault();
        }

    }
}
