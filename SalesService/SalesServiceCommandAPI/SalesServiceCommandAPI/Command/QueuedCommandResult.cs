﻿using SalesServiceCommandAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceCommandAPI.Command
{
    public class QueuedCommandResult <GenericEntity> where GenericEntity : Entity
    {

        public bool isSuccess { get; private set; }

        public GenericEntity entity { get; internal set; }

        public QueuedCommandResult(bool isSuccess, GenericEntity entity)
        {
            this.isSuccess = isSuccess;
            this.entity = entity;
        }

    }
}
