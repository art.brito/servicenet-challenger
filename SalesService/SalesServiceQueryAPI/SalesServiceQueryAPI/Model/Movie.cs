﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceQueryAPI.Model
{
    public class Movie
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string mongoID { get; set; }

        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("title")]
        public string title { get; set; }

        [BsonElement("subtitle")]
        public string subTitle { get; set; }

        [BsonElement("releasedate")]
        public string releaseDate { get; set; }

        public Movie()
        {
        }

        public Movie(string id, string title, string subTitle, string releaseDate)
        {
            this.id = id;
            this.title = title;
            this.subTitle = subTitle;
            this.releaseDate = releaseDate;
        }
    }
}
