﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceCommandAPI.Model
{
    public class Movie : Entity
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Guid id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string title { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string subTitle { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? releaseDate { get; set; }

        public Movie()
        {
        }

        public Movie(string id)
        {
            this.id = new Guid(id);
        }

        public Movie(Guid id, string title, string subTitle, DateTime releaseDate)
        {
            this.id = id;
            this.title = title;
            this.subTitle = subTitle;
            this.releaseDate = releaseDate;
        }

    }
}
