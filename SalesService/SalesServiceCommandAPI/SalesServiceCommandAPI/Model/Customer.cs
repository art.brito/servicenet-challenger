﻿using SalesServiceCommandAPI.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceCommandAPI.Model
{
    public class Customer : Entity
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Guid id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int Age { get; set; }

        public Customer()
        {
        }

        public Customer(string id)
        {
            this.id = new Guid(id);
        }

        public Customer(Guid id, string name, int age)
        {
            this.id = id;
            Name = name;
            Age = age;
        }

    }
}
