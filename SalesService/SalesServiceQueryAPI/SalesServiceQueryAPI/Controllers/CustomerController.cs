﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesServiceQueryAPI.Model;
using SalesServiceQueryAPI.Service;

namespace SalesServiceQueryAPI.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private CustomerService service;

        public CustomerController(CustomerService service)
        {
            this.service = service;
        }

        [HttpGet]
        public List<Customer> GetMovies()
        {
            return service.GetMovies();
        }

        [HttpGet("{id}")]
        public Customer GetMovie(string id)
        {
            return service.GetMovie(id);
        }
    }
}