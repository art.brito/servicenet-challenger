﻿using SalesServiceQueryAPI.Model;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceQueryAPI.DataBase
{
    public class DatabaseContext
    {
        private static DatabaseContext singletonInstanceContext;
        
        private readonly IMongoDatabase _database = null;

        private readonly string ConnectionString = "mongodb://localhost:27017";
        private readonly string DataBase = "crud-cache";

        public DatabaseContext()
        {
            var client = new MongoClient(this.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(this.DataBase);
        }

        public IMongoCollection<Movie> Movie
        {
            get
            {
                return _database.GetCollection<Movie>("movie");
            }
        }

        public IMongoCollection<Customer> Customer
        {
            get
            {
                return _database.GetCollection<Customer>("customer");
            }
        }

        public static DatabaseContext getInstance()
        {
            if (singletonInstanceContext == null)
            {
                singletonInstanceContext = new DatabaseContext();
            }
            return singletonInstanceContext;
        }
    }
}
