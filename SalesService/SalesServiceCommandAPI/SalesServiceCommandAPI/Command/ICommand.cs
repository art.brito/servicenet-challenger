﻿using SalesServiceCommandAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceCommandAPI.Command
{
    public interface ICommand<GenericEntity> where GenericEntity : Entity
    {

        GenericEntity GetPayload();

    }
}
