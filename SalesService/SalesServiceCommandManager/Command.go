package main

type CommandType int

/***/

const (
	CREATE CommandType = 0
	UPDATE CommandType = 1
	DELETE CommandType = 2
)

/***/

type MovieCommand struct {
	Movie struct {
		ID          string `json:"id"`
		Title       string `json:"title"`
		SubTitle    string `json:"subTitle"`
		ReleaseDate string `json:"releaseDate"`
	} `json:"movie"`
	CommandType CommandType `json:"commandType"`
}

type CustomerCommand struct {
	Customer struct {
		ID   string `json:"id"`
		Name string `json:"name"`
		Age  int    `json:"age"`
	} `json:"customer"`
	CommandType CommandType `json:"commandType"`
}
