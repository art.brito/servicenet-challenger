﻿using SalesServiceCommandAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceCommandAPI.Command
{
    public class CreateMovieCommand : ICommand<Movie>
    {

        public Movie movie { get; set; }

        public CommandType commandType { get; set; }

        public CreateMovieCommand(Movie movie)
        {
            this.movie = movie;
            this.movie.id = Guid.NewGuid();
            this.commandType = CommandType.CREATE;
        }

        public Movie GetPayload()
        {
            return this.movie;
        }
    }
}
