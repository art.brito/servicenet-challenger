﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SalesServiceQueryAPI.Model;
using SalesServiceQueryAPI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SalesServiceQueryAPI.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {

        private MovieService service;

        public MovieController(MovieService service)
        {
            this.service = service;
        }

        [HttpGet]
        public List<Movie> GetMovies()
        {
            return service.GetMovies();
        }

        [HttpGet("{id}")]
        public Movie GetMovie(string id)
        {
            return service.GetMovie(id);
        }

    }
}