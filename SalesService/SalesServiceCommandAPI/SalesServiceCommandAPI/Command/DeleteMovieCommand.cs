﻿using SalesServiceCommandAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceCommandAPI.Command
{
    public class DeleteMovieCommand : ICommand<Movie>
    {
        public Movie movie;

        public CommandType commandType { get; set; }

        public DeleteMovieCommand(string id)
        {
            this.movie = new Movie(id);
            this.commandType = CommandType.DELETE;
        }

        public Movie GetPayload()
        {
            return this.movie;
        }
    }
}
