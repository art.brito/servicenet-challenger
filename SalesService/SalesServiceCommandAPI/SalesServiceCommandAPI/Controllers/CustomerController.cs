﻿using System;
using SalesServiceCommandAPI.Command;
using Microsoft.AspNetCore.Mvc;
using SalesServiceCommandAPI.Model;
using SalesServiceCommandAPI.Service;

namespace SalesServiceCommandAPI.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        
        private CustomerService service;

        public CustomerController()
        {
            this.service = new CustomerService();
        }

        [HttpPost]
        public Customer CreateCustomer([FromBody] Customer customer)
        {
            CreateCustomerCommand createMovieCommand = new CreateCustomerCommand(customer);

            QueuedCommandResult<Customer> commandResult = service.sendCommand(createMovieCommand);

            if (commandResult.isSuccess)
            {
                return commandResult.entity;
            }
            else
            {
                throw new Exception("Erro ao cadastrar o cliente");
            }
        }

        [HttpPut("{id}")]
        public Customer UpdateCustomer(string id, [FromBody] Customer customer)
        {
            customer.id = new Guid(id);

            UpdateCustomerCommand updateMovieCommand = new UpdateCustomerCommand(customer);

            QueuedCommandResult<Customer> commandResult = service.sendCommand(updateMovieCommand);

            if (commandResult.isSuccess)
            {
                return commandResult.entity;
            }
            else
            {
                throw new Exception("Erro ao atualizar o cadastro do cliente");
            }
        }

        [HttpDelete("{id}")]
        public void DeleteCustomer(string id)
        {
            DeleteCustomerCommand deleteMovieCommand = new DeleteCustomerCommand(id);

            QueuedCommandResult<Customer> commandResult = service.sendCommand(deleteMovieCommand);

            if (!commandResult.isSuccess)
            {
                throw new Exception("Erro ao deletar o cliente");
            }
        }

    }
}