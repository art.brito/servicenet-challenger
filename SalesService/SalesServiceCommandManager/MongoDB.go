package main

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func updateCache(command MovieCommand) {
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	failOnError(err, "Erro ao conectar-se ao banco de cache")

	err = client.Ping(context.TODO(), nil)
	failOnError(err, "Erro ao conectar-se ao banco de cache")

	collection := client.Database("crud-cache").Collection("movie")

	switch command.CommandType {
	case CREATE:
		collection.InsertOne(context.TODO(), command.Movie)
	case UPDATE:
		filter := bson.M{"id": command.Movie.ID}

		updateEntity := bson.D{}

		updateEntity = append(updateEntity, bson.E{"id", command.Movie.ID})

		if command.Movie.Title != "" {
			updateEntity = append(updateEntity, bson.E{"title", command.Movie.Title})
		}

		if command.Movie.SubTitle != "" {
			updateEntity = append(updateEntity, bson.E{"subtitle", command.Movie.SubTitle})
		}

		if command.Movie.ReleaseDate != "" {
			updateEntity = append(updateEntity, bson.E{"releasedate", command.Movie.ReleaseDate})
		}

		fmt.Println(updateEntity)

		update := bson.D{
			{"$set",
				updateEntity,
			},
		}

		collection.UpdateOne(context.TODO(), filter, update)
	case DELETE:
		filter := bson.M{"id": command.Movie.ID}
		collection.DeleteOne(context.TODO(), filter)
	}

	err = client.Disconnect(context.TODO())
	failOnError(err, "Erro ao conectar-se ao banco de cache")
}

func updateEventSourceStored(command MovieCommand) {
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	failOnError(err, "Erro ao conectar-se ao banco de cache")

	err = client.Ping(context.TODO(), nil)
	failOnError(err, "Erro ao conectar-se ao banco de cache")

	collection := client.Database("crud-eventsource").Collection("movie")
	collection.InsertOne(context.TODO(), command)

	err = client.Disconnect(context.TODO())
	failOnError(err, "Erro ao conectar-se ao banco de cache")
}
