﻿using SalesServiceCommandAPI.Command;
using SalesServiceCommandAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceCommandAPI.Command
{
    public class CreateCustomerCommand : ICommand<Customer>
    {

        public Customer customer { get; set; }

        public CommandType commandType { get; set; }

        public CreateCustomerCommand(Customer customer)
        {
            this.customer = customer;
            this.customer.id = Guid.NewGuid();
            this.commandType = CommandType.CREATE;
        }

        public Customer GetPayload()
        {
            return this.customer;
        }

    }
}
