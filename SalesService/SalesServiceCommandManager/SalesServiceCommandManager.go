package main

import (
	"amqp"
	"encoding/json"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"SALES_SERVICE_COMMAND_QUEUE", // name
		false,                         // durable
		false,                         // delete when unused
		false,                         // exclusive
		false,                         // no-wait
		nil,                           // arguments
	)
	failOnError(err, "Failed to declare a queue")

	forever := make(chan bool)

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	go func() {
		for commandMessage := range msgs {

			fmt.Println(string(commandMessage.Body))

			var command MovieCommand
			err := json.Unmarshal(commandMessage.Body, &command)
			failOnError(err, "Erro ao deserializar o json")

			updateCache(command)
			updateEventSourceStored(command)
			updateRelationalDatabase(command)
		}
	}()

	<-forever
}
