﻿using SalesServiceCommandAPI.Model;

namespace SalesServiceCommandAPI.Command
{
    public class UpdateCustomerCommand : ICommand<Customer>
    {

        private Customer customer;

        public CommandType commandType { get; set; }

        public UpdateCustomerCommand(Customer customer)
        {
            this.customer = customer;
            this.commandType = CommandType.UPDATE;
        }

        public Customer GetPayload()
        {
            return this.customer;
        }

    }
}
