﻿using SalesServiceCommandAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceCommandAPI.Command
{
    public class UpdateMovieCommand : ICommand<Movie>
    {

        public Movie movie;

        public CommandType commandType { get; set; }

        public UpdateMovieCommand(Movie movie)
        {
            this.movie = movie;
            this.commandType = CommandType.UPDATE;
        }

        public Movie GetPayload()
        {
            return this.movie;
        }
    }
}
