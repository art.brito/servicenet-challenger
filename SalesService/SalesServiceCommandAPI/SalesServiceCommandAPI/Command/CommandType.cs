﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesServiceCommandAPI.Command
{
    public enum CommandType
    {
        CREATE,
        UPDATE,
        DELETE
    }
}
