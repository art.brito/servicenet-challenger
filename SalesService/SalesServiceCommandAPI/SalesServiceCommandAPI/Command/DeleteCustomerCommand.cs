﻿using SalesServiceCommandAPI.Model;

namespace SalesServiceCommandAPI.Command
{
    public class DeleteCustomerCommand : ICommand<Customer>
    {

        public Customer customer;

        public CommandType commandType { get; set; }

        public DeleteCustomerCommand(string id)
        {
            this.customer = new Customer(id);
            this.commandType = CommandType.DELETE;
        }

        public Customer GetPayload()
        {
            return this.customer;
        }

    }
}
