﻿using SalesServiceCommandAPI.Command;
using SalesServiceCommandAPI.Model;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

namespace SalesServiceCommandAPI.Service
{
    public class MovieService
    {

        private const string movieQueueCommand = "SALES_SERVICE_COMMAND_QUEUE";

        public QueuedCommandResult<Movie> sendCommand(ICommand<Movie> createMovieCommand)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: movieQueueCommand,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                string bodyMessage = JsonConvert.SerializeObject(createMovieCommand);
                var body = Encoding.UTF8.GetBytes(bodyMessage);

                channel.BasicPublish(exchange: "",
                                     routingKey: movieQueueCommand,
                                     basicProperties: null,
                                     body: body);
            }

            return new QueuedCommandResult<Movie>(true, createMovieCommand.GetPayload());
        }
    }
}
